// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

let x = canvas.width / 2;
var y = canvas.height / 2;

let dx = 1 + Math.floor(Math.random() * 19);
let dy = 1 + Math.floor(Math.random() * 19);

const radius = 10;

const twoPI = Math.PI*2;

// Recount position

function recountPosition() {
  let newX = x + dx;
  if (newX > canvas.width - radius) {
    newX = 2 * (canvas.width - radius) - newX;
    dx = -dx;
  } else if (newX < radius) {
    newX = 2 * radius - newX;
    dx = -dx;
  }

  x = newX;

  let newY = y + dy;
  if (newY > canvas.height - radius) {
    newY = 2 * (canvas.height - radius) - newY;
    dy = -dy;
  } else if (newY < radius) {
    newY = 2 * radius - newY;
    dy = -dy;
  }

  y = newY;
};

// Draw

function drawCircle() {
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, twoPI);
  ctx.strokeStyle = "#0095DD";
  ctx.stroke();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawCircle();

  recountPosition();

  requestAnimationFrame(draw);
}

draw();
