// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const circlesCount = 30;

const radius = 10;

const twoPI = Math.PI*2;

// generate circles

const circles = [];
for (let i = 0; i < circlesCount; ++i) {
  circles.push({
    x: radius + Math.floor(Math.random() * (canvas.width - 2 * radius)),
    y: radius + Math.floor(Math.random() * (canvas.height - 2 * radius)),
    vx: 1 + Math.floor(Math.random() * 19),
    vy: 1 + Math.floor(Math.random() * 19),
  });
}

// Recount position

function recountPosition(circle) {
  let newX = circle.x + circle.vx;
  if (newX > canvas.width - radius) {
    newX = 2 * (canvas.width - radius) - newX;
    circle.vx = -circle.vx;
  } else if (newX < radius) {
    newX = 2 * radius - newX;
    circle.vx = -circle.vx;
  }

  circle.x = newX;

  let newY = circle.y + circle.vy;
  if (newY > canvas.height - radius) {
    newY = 2 * (canvas.height - radius) - newY;
    circle.vy = -circle.vy;
  } else if (newY < radius) {
    newY = 2 * radius - newY;
    circle.vy = -circle.vy;
  }

  circle.y = newY;
};

// Draw

function drawCircle(circle) {
  ctx.beginPath();
  ctx.arc(circle.x, circle.y, radius, 0, twoPI);
  ctx.strokeStyle = "#0095DD";
  ctx.stroke();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  circles.forEach(drawCircle);
  circles.forEach(recountPosition);

  requestAnimationFrame(draw);
}

draw();
