// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const cellWidth = 20;
const cellHeight = 20;

const rowLength = Math.floor(canvas.width / cellWidth);
const rowsNumber = Math.floor(canvas.height / cellHeight);

let infusorias = [];
for (let i = 0; i < rowsNumber; ++i) {
  const infusoriasRow = [];

  for (let j = 0; j < rowLength; ++j) {
    infusoriasRow.push(Math.random() < 0.2);
  }

  infusorias.push(infusoriasRow);
}

// Recount

function recountInfusorias() {
  const newInfusorias = [];

  for (let i = 0; i < rowsNumber; ++i) {
    const newInfusoriasRow = [];

    for (let j = 0; j < rowLength; ++j) {
      let neighborsCount = 0;

      for (let k = Math.max(0, i - 1); k < Math.min(rowsNumber, i + 2); ++k) {
        for (let l = Math.max(0, j - 1); l < Math.min(rowLength, j + 2); ++l) {
          if (infusorias[k][l] && (k !== i || l !== j)) {
            ++neighborsCount;
          }
        }
      }

      if (infusorias[i][j]) {
        newInfusoriasRow.push(neighborsCount === 2 || neighborsCount === 3);
      } else {
        newInfusoriasRow.push(neighborsCount === 3);
      }
    }

    newInfusorias.push(newInfusoriasRow);
  }

  infusorias = newInfusorias;
};

// Draw

function drawInfusorias() {
  for (let i = 0; i < rowsNumber; ++i) {
    for (let j = 0; j < rowLength; ++j) {
      const infusoria = infusorias[i][j];

      ctx.beginPath();
      ctx.rect(j * cellWidth, i * cellHeight, cellWidth, cellHeight);
      ctx.fillStyle = infusoria ? '#0095DD' : '#eeeeee';
      ctx.fill();
      ctx.closePath();
    }
  }
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawInfusorias();
  recountInfusorias();

  setTimeout(draw, 250);
}

draw();
