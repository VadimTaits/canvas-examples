// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const moleculesCount = 60;
let leftCount = Math.floor(moleculesCount / 2);
let rightCount = moleculesCount - leftCount;

const moleculeRadius = 2;

const twoPI = Math.PI*2;

let tempeartureLeft = Math.floor(6 * moleculesCount / 2);
let tempeartureRight = Math.floor(2 * moleculesCount / 2);

const halfCanvasWidth = canvas.width / 2;

const maxTemperature = tempeartureLeft + tempeartureRight;
const maxTemperatureForCountColor = maxTemperature / 255;

const spaceHeight = 60;

const borderHeight = Math.round((canvas.height - spaceHeight) / 2);
const downBorderY = borderHeight + spaceHeight;

const spaceTopY = borderHeight + moleculeRadius;
const spaceBottomY = downBorderY - moleculeRadius;

// generate molecules

const molecules = [];
for (let i = 0; i < moleculesCount; ++i) {
  const isLeft = i < moleculesCount / 2;

  molecules.push({
    x: isLeft ?
      moleculeRadius + Math.floor(Math.random() * (halfCanvasWidth - 2 * moleculeRadius)) :
      halfCanvasWidth + moleculeRadius + Math.floor(Math.random() * (halfCanvasWidth - 2 * moleculeRadius)),
    y: moleculeRadius + Math.floor(Math.random() * (canvas.height - 2 * moleculeRadius)),
    angle: Math.random() * twoPI,
    isLeft,
  });
}

// Recount position

function recountMoleculePosition(molecule) {
  const speed = molecule.isLeft ?
    tempeartureLeft / leftCount :
    tempeartureRight / rightCount;

  const vx = speed * Math.cos(molecule.angle);
  const vy = speed * Math.sin(molecule.angle);

  let newX = molecule.x + vx;
  if (newX > canvas.width - moleculeRadius) {
    newX = 2 * (canvas.width - moleculeRadius) - newX;
    molecule.angle = Math.PI - molecule.angle;
  } else if (newX < moleculeRadius) {
    newX = 2 * moleculeRadius - newX;
    molecule.angle = Math.PI - molecule.angle;
  }

  let newY = molecule.y + vy;
  if (newY > canvas.height - moleculeRadius) {
    newY = 2 * (canvas.height - moleculeRadius) - newY;
    molecule.angle = -molecule.angle;
  } else if (newY < moleculeRadius) {
    newY = 2 * moleculeRadius - newY;
    molecule.angle = -molecule.angle;
  }

  if (molecule.isLeft) {
    if (
      newX > halfCanvasWidth - moleculeRadius &&
      (
        newY < spaceTopY || newY > spaceBottomY
      )
    ) {
      newX = 2 * (halfCanvasWidth - moleculeRadius) - newX;
      molecule.angle = Math.PI - molecule.angle;
    }
  } else {
    if (
      newX < halfCanvasWidth + moleculeRadius &&
      (
        newY < spaceTopY || newY > spaceBottomY
      )
    ) {
      newX = 2 * (halfCanvasWidth + moleculeRadius) - newX;
      molecule.angle = Math.PI - molecule.angle;
    }
  }

  const newIsLeft = newX < halfCanvasWidth;

  molecule.x = newX;
  molecule.y = newY;

  if (newIsLeft !== molecule.isLeft) {
    if (newIsLeft) {
      const tempeartureDiff = tempeartureRight / rightCount;

      ++leftCount;
      --rightCount;

      tempeartureLeft += tempeartureDiff;
      tempeartureRight -= tempeartureDiff;
    } else {
      const tempeartureDiff = tempeartureLeft / leftCount;

      --leftCount;
      ++rightCount;

      tempeartureLeft -= tempeartureDiff;
      tempeartureRight += tempeartureDiff;
    }

    molecule.isLeft = newIsLeft;
  }
};

// Draw

function drawMolecule(molecule) {
  const red = Math.round(
    (
      molecule.isLeft ?
        tempeartureLeft :
        tempeartureRight
    ) / maxTemperatureForCountColor
  );

  const blue = 255 - red;

  ctx.beginPath();
  ctx.arc(molecule.x, molecule.y, moleculeRadius, 0, twoPI);
  ctx.strokeStyle = `rgb(${red},0,${blue})`;
  ctx.stroke();
  ctx.closePath();
};

function drawBorders() {
  ctx.strokeStyle='black';

  ctx.beginPath();
  ctx.moveTo(halfCanvasWidth, 0);
  ctx.lineTo(halfCanvasWidth, borderHeight);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(halfCanvasWidth, downBorderY);
  ctx.lineTo(halfCanvasWidth, canvas.height);
  ctx.stroke();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  molecules.forEach(drawMolecule);
  drawBorders();

  molecules.forEach(recountMoleculePosition);

  requestAnimationFrame(draw);
}

draw();
