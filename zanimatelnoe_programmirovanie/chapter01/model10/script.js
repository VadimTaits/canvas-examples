// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

let x = 0;
let y = 0;
let angle = 0;

const twoPI = Math.PI*2;

let isPenDown = false;

// turtle api

const turtle = {
  init() {
    x = 0;
    y = canvas.height;
    angle = Math.PI / 2;
    isPenDown = false;
  },

  go(distance) {
    const newX = x + distance * Math.cos(angle);
    const newY = y - distance * Math.sin(angle);

    if (isPenDown) {
      ctx.strokeStyle='black';

      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(newX, newY);
      ctx.stroke();
    }

    x = newX;
    y = newY;
  },

  turn(dAngle) {
    angle += dAngle * Math.PI / 180;

    if (angle > twoPI) {
      angle -= twoPI;
    } else if (angle < -twoPI) {
      angle -= twoPI;
    }
  },

  penDown() {
    isPenDown = true;
  },

  penUp() {
    isPenDown = false;
  },
};



// Draw

function draw1() {
  turtle.init();

  turtle.turn(-45);
  turtle.go(220);

  turtle.penDown();
  let len = 1;
  for (let i = 0; i < 40; ++i) {
    turtle.go(Math.round(len));
    turtle.turn(90);
    len += 5;
  }
}

function draw2() {
  turtle.init();

  turtle.turn(-55);
  turtle.go(500);

  turtle.penDown();
  let len = 1;
  for (let i = 0; i < 300; ++i) {
    turtle.go(Math.round(len));
    turtle.turn(10);
    len += 0.1;
  }
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  draw1();
  draw2();
}

draw();
