// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const sunRadius = 60;
const earthRadius = 20;
const moonRadius = 5;

const sunX = canvas.width / 2;
const sunY = canvas.height / 2;

const sunEarthDistance = 180;
const earthMoonDistance = 50;

let earthAlpha = 0;
let moonAlpha = 0;

let earthAlphaDiff = Math.PI / 64;
let moonAlphaDiff = Math.PI / 12;

const twoPI = Math.PI*2;

// Recount position

function recountPositions() {
  earthAlpha += earthAlphaDiff;
  if (earthAlpha > twoPI) {
    earthAlpha -= twoPI;
  }

  moonAlpha += moonAlphaDiff;
  if (moonAlpha > twoPI) {
    moonAlpha -= twoPI;
  }
};

// Draw

function drawAll() {
  ctx.beginPath();
  ctx.arc(sunX, sunY, sunRadius, 0, twoPI);
  ctx.strokeStyle = "#FF9922";
  ctx.stroke();
  ctx.closePath();

  const earthDX = sunEarthDistance * Math.cos(earthAlpha);
  const earthDY = sunEarthDistance * Math.sin(earthAlpha);

  const earthX = sunX + earthDX;
  const earthY = sunY + earthDY;

  ctx.beginPath();
  ctx.arc(earthX, earthY, earthRadius, 0, twoPI);
  ctx.strokeStyle = "#33EE33";
  ctx.stroke();
  ctx.closePath();

  const moonDX = earthMoonDistance * Math.cos(earthAlpha + moonAlpha);
  const moonDY = earthMoonDistance * Math.sin(earthAlpha + moonAlpha);

  ctx.beginPath();
  ctx.arc(earthX + moonDX, earthY + moonDY, moonRadius, 0, twoPI);
  ctx.strokeStyle = "#333333";
  ctx.stroke();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawAll();

  recountPositions();

  requestAnimationFrame(draw);
}

draw();
