// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const moleculesCount = 50;

const moleculeRadius = 2;

const twoPI = Math.PI*2;
const koef = 0.1 + Math.random() * 0.8;

const brownianRadius = 60;

let brownianX = brownianRadius + Math.floor(Math.random() * (canvas.width - 2 * brownianRadius));
let brownianY = brownianRadius + Math.floor(Math.random() * (canvas.height - 2 * brownianRadius));

let brownianVX = 0;
let brownianVY = 0;

const radiusSum = moleculeRadius + brownianRadius;
const radiusSumSquare = radiusSum * radiusSum;

// generate molecules

const molecules = [];
for (let i = 0; i < moleculesCount; ++i) {
  molecules.push({
    x: moleculeRadius + Math.floor(Math.random() * (canvas.width - 2 * moleculeRadius)),
    y: moleculeRadius + Math.floor(Math.random() * (canvas.height - 2 * moleculeRadius)),
    vx: 1 + Math.floor(Math.random() * 9),
    vy: 1 + Math.floor(Math.random() * 9),
    inCollision: false,
  });
}

// Recount position

function recountBrownianPosition() {
  let newX = brownianX + brownianVX;
  if (newX > canvas.width - brownianRadius) {
    newX = 2 * (canvas.width - brownianRadius) - newX;
    brownianVX = -brownianVX;
  } else if (newX < brownianRadius) {
    newX = 2 * brownianRadius - newX;
    brownianVX = -brownianVX;
  }

  brownianX = newX;

  let newY = brownianY + brownianVY;
  if (newY > canvas.height - brownianRadius) {
    newY = 2 * (canvas.height - brownianRadius) - newY;
    brownianVY = -brownianVY;
  } else if (newY < brownianRadius) {
    newY = 2 * brownianRadius - newY;
    brownianVY = -brownianVY;
  }

  brownianY = newY;
};

function recountMoleculePosition(molecule) {
  let newX = molecule.x + molecule.vx;
  if (newX > canvas.width - moleculeRadius) {
    newX = 2 * (canvas.width - moleculeRadius) - newX;
    molecule.vx = -molecule.vx;
  } else if (newX < moleculeRadius) {
    newX = 2 * moleculeRadius - newX;
    molecule.vx = -molecule.vx;
  }

  molecule.x = newX;

  let newY = molecule.y + molecule.vy;
  if (newY > canvas.height - moleculeRadius) {
    newY = 2 * (canvas.height - moleculeRadius) - newY;
    molecule.vy = -molecule.vy;
  } else if (newY < moleculeRadius) {
    newY = 2 * moleculeRadius - newY;
    molecule.vy = -molecule.vy;
  }

  molecule.y = newY;
};

function checkHit(molecule) {
  const dx = molecule.x - brownianX;
  const dy = molecule.y - brownianY;

  const isHit = dx * dx + dy * dy < radiusSumSquare;

  if (isHit) {
    if (!molecule.inCollision) {
      molecule.inCollision = true;
      brownianVX += molecule.vx * koef;
      brownianVY += molecule.vy * koef;
    }
  } else {
    if (molecule.inCollision) {
      molecule.inCollision = false;
    }
  }
};

// Draw

function drawBrownian() {
  ctx.beginPath();
  ctx.arc(brownianX, brownianY, brownianRadius, 0, twoPI);
  ctx.strokeStyle = "#EE0000";
  ctx.stroke();
  ctx.closePath();
};

function drawMolecule(molecule) {
  ctx.beginPath();
  ctx.arc(molecule.x, molecule.y, moleculeRadius, 0, twoPI);
  ctx.strokeStyle = "#0095DD";
  ctx.stroke();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawBrownian();
  molecules.forEach(drawMolecule);

  molecules.forEach(recountMoleculePosition);
  molecules.forEach(checkHit);
  recountBrownianPosition();

  requestAnimationFrame(draw);
}

draw();
