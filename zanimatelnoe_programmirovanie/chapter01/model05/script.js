// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const radius = 10;

let x = radius + Math.round(Math.random() * (canvas.width - radius));
var y = radius + Math.round(Math.random() * canvas.height / 2);

let vx = Math.round(Math.random() * 10);
let vy = 0;

const k = 0.1 + Math.random() * 0.8;

const a = 2;

const twoPI = Math.PI*2;

// Recount position

function recountPosition() {
  let newX = x + vx;
  if (newX > canvas.width - radius) {
    newX = 2 * (canvas.width - radius) - newX;
    vx = -vx;
  } else if (newX < radius) {
    newX = 2 * radius - newX;
    vx = -vx;
  }

  x = newX;

  let newY = y + vy;
  if (newY > canvas.height - radius) {
    newY = 2 * (canvas.height - radius) - newY;
    vy = -Math.floor(vy * k);
  } else if (newY < radius) {
    newY = 2 * radius - newY;
    vy = -vy;
  }

  y = newY;

  if (canvas.height - radius - newY > a) {
    vy += a;
  }
};

// Draw

function drawCircle() {
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, twoPI);
  ctx.fillStyle = "#FF3333";
  ctx.fill();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawCircle();

  recountPosition();

  requestAnimationFrame(draw);
}

draw();
