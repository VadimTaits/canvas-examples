const totalEl = document.getElementById('total');
const currentEl = document.getElementById('current');
const averageEl = document.getElementById('average');

let pointsTotal = 0;
let pointsInside = 0;

let current = null;
let average = null;

function iter() {
  const x = Math.random() - 0.5;
  const y = Math.random() - 0.5;

  const isInside = x * x + y * y < 0.25;

  if (isInside) {
    ++pointsInside;
  }

  ++pointsTotal;

  current = 4 * pointsInside / pointsTotal;

  average = average === null ?
    current :
    ((average * (pointsTotal - 1) + current) / pointsTotal);

  totalEl.textContent = pointsTotal;
  currentEl.textContent = current;
  averageEl.textContent = average;

  setTimeout(iter, 10);
};

iter();
