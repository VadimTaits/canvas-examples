// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const mazeWidth = 10;
const mazeHeight = 8;

function brealWall(mazeForBreakWall, x, y, dx, dy) {
  if (dx === -1) {
    mazeForBreakWall[y][x].wallLeft = false;
  } else if (dx === 1) {
    mazeForBreakWall[y][x + 1].wallLeft = false;
  } else if (dy === -1) {
    mazeForBreakWall[y][x].wallTop = false;
  } else if (dy === 1) {
    mazeForBreakWall[y + 1][x].wallTop = false;
  }
}

function primGenerateMaze() {
  const dx = [1, 0, -1, 0];
  const dy = [0, -1, 0, 1];

  const rawMaze = [];
  const resMaze = [];
  for (let j = 0; j < mazeHeight; ++j) {
    const rawMazeRow = [];
    const resMazeRow = [];

    for (let i = 0; i < mazeWidth; ++i) {
      rawMazeRow.push('outside');
      resMazeRow.push({
        wallTop: true,
        wallLeft: true,
      });
    }

    rawMaze.push(rawMazeRow);
    resMaze.push(resMazeRow);
  }

  const startX = Math.floor(Math.random() * mazeWidth);
  const startY = Math.floor(Math.random() * mazeHeight);

  let bordersCount = 0;

  rawMaze[startY][startX] = 'inside';
  for (let i = 0; i < 4; ++i) {
    const borderX = startX + dx[i];
    const borderY = startY + dy[i];

    if (borderX >= 0 && borderX < mazeWidth && borderY >= 0 && borderY < mazeHeight) {
      rawMaze[borderY][borderX] = 'border';
      ++bordersCount;
    }
  }

  for (; bordersCount > 0;) {
    let borderIndexForProcess = Math.floor(Math.random() * bordersCount);
    let borderX;
    let borderY;

    for (let j = 0, currentBorder = -1, found = false; !found && j < mazeHeight; ++j) {
      for (let i = 0; !found && i < mazeWidth; ++i) {
        if (rawMaze[j][i] === 'border') {
          ++currentBorder;

          if (currentBorder === borderIndexForProcess) {
            found = true;
            borderX = i;
            borderY = j;
          }
        }
      }
    }

    rawMaze[borderY][borderX] = 'inside';
    --bordersCount;

    const allowedDirections = [];
    for (let i = 0; i < 4; ++i) {
      const neighborX = borderX + dx[i];
      const neighborY = borderY + dy[i];

      if (neighborX >= 0 && neighborX < mazeWidth && neighborY >= 0 && neighborY < mazeHeight) {
        const currentRawValue = rawMaze[neighborY][neighborX];

        switch (currentRawValue) {
          case 'inside':
            allowedDirections.push(i);
            break;

          case 'outside':
            rawMaze[neighborY][neighborX] = 'border';
            ++bordersCount;

          default:
            break;
        }
      }
    }

    const direction = allowedDirections[Math.floor(Math.random() * allowedDirections.length)];

    brealWall(resMaze, borderX, borderY, dx[direction], dy[direction]);
  }

  return resMaze;
}

const maze = primGenerateMaze();

const cellWidth = 50;
const pathStepRadius = 20;

const twoPI = Math.PI * 2;

// Solve

const marked = [];
function setMark(x, y, value) {
  if (!marked[y]) {
    marked[y] = [];
  }

  marked[y][x] = value;
};

function getMark(x, y) {
  if (!marked[y]) {
    return null;
  }

  return marked[y][x] || null;
};

const startX = Math.floor(Math.random() * mazeWidth);
const startY = Math.floor(Math.random() * mazeHeight);
const targetX = Math.floor(Math.random() * mazeWidth);
const targetY = Math.floor(Math.random() * mazeHeight);
const dxMethods = [1, 0, -1, 0];
const dyMethods = [0, -1, 0, 1];
const path = [];

function canGo(x, y, dx, dy) {
  if (dx === -1) {
    return x > 0 && !maze[y][x].wallLeft;
  }

  if (dx === 1) {
    return x < mazeWidth - 1 && !maze[y][x + 1].wallLeft;
  }

  if (dy === -1) {
    return y > 0 && !maze[y][x].wallTop;
  }

  if (dy === 1) {
    return y < mazeHeight - 1 && !maze[y + 1][x].wallTop;
  }

  throw new Error(`Unknown direction (${ dx }, ${ dy })`);
};

function markSolveStep(x, y, depth) {
  setMark(x, y, depth);

  const newMark = depth + 1;

  for (let i = 0; i < 4; ++i) {
    const newX = x + dxMethods[i];
    const newY = y + dyMethods[i];

    const oldMark = getMark(newX, newY);

    if (
      (oldMark === null || newMark < oldMark) &&
      canGo(x, y, dxMethods[i], dyMethods[i])
    ) {
      markSolveStep(newX, newY, newMark);
    }
  }
};

function getPath(x, y, path) {
  if (x === startX && y === startY) {
    return path;
  }

  const currentMark = getMark(x, y);

  for (let i = 0; i < 4; ++i) {
    const newX = x + dxMethods[i];
    const newY = y + dyMethods[i];

    const mark = getMark(newX, newY);

    if (
      mark === currentMark - 1 &&
      canGo(x, y, dxMethods[i], dyMethods[i])
    ) {
      return getPath(newX, newY, [{
        x: newX,
        y: newY
      }].concat(path));
    }
  }
};

function markSolve() {
  markSolveStep(startX, startY, 1);

  if (getMark(targetX, targetY)) {
    return {
      path: getPath(targetX, targetY, [{
        x: targetX,
        y: targetY,
      }]),
      solved: true,
    };
  }

  return {
    solved: false,
  };
};

// Draw

function drawMaze() {
  ctx.beginPath();
  ctx.strokeStyle = 'black';

  for (let i = 0; i < mazeWidth; ++i) {
    for (let j = 0; j < mazeHeight; ++j) {
      const mazeCell = maze[j][i];

      const cornerX = i * cellWidth;
      const cornerY = j * cellWidth;

      if (mazeCell.wallLeft) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX, cornerY + cellWidth);
        ctx.stroke();
      }

      if (mazeCell.wallTop) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX + cellWidth, cornerY);
        ctx.stroke();
      }
    }
  }

  const downRightCornerX = mazeWidth * cellWidth;
  const downRightCornerY = mazeHeight * cellWidth;

  ctx.moveTo(0, downRightCornerY);
  ctx.lineTo(downRightCornerX, downRightCornerY);
  ctx.lineTo(downRightCornerX, 0);
  ctx.stroke();

  ctx.closePath();
};

function drawPath(path) {
  ctx.fillStyle = '#FF0000';
  ctx.strokeStyle = '#FF0000';
  path.forEach(({ x, y }) => {
    const cornerX = (x + 0.5) * cellWidth;
    const cornerY = (y + 0.5) * cellWidth;

    ctx.beginPath();
    ctx.arc(cornerX, cornerY, pathStepRadius, 0, twoPI);
    if (
      x === startX && y === startY ||
      x === targetX && y === targetY
    ) {
      ctx.fill();
    } else {
      ctx.stroke();
    }
    ctx.closePath();
  });
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawMaze();

  const solveResult = markSolve();

  if (solveResult.solved) {
    drawPath(solveResult.path);
  }
}

draw();
