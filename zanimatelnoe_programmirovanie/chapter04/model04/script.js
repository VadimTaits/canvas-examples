// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const mazeWidth = 10;
const mazeHeight = 8;

const startX = Math.floor(Math.random() * mazeWidth);
const startY = Math.floor(Math.random() * mazeHeight);
const targetX = Math.floor(Math.random() * mazeWidth);
const targetY = Math.floor(Math.random() * mazeHeight);
const dxMethods = [1, 0, -1, 0];
const dyMethods = [0, -1, 0, 1];

function breakWall(mazeForBreakWall, x, y, dx, dy) {
  if (dx === -1) {
    mazeForBreakWall[y][x].wallLeft = false;
  } else if (dx === 1) {
    mazeForBreakWall[y][x + 1].wallLeft = false;
  } else if (dy === -1) {
    mazeForBreakWall[y][x].wallTop = false;
  } else if (dy === 1) {
    mazeForBreakWall[y + 1][x].wallTop = false;
  }
}

const rowWallsLength = 2 * mazeWidth - 1;
function getCellByWallIndex(maze, wallIndex) {
  const wallRow = Math.floor(wallIndex / rowWallsLength);
  const wallIndexInRow = wallIndex - wallRow * rowWallsLength;

  if (wallIndexInRow < mazeWidth) {
    return {
      x: wallIndexInRow,
      y: wallRow,
      dx: -1,
      dy: 0,
    };
  }

  return {
    x: wallIndexInRow - mazeWidth,
    y: wallRow + 1,
    dx: 0,
    dy: -1,
  };
}

function kruskalGenerateMaze() {
  const wallsOrder = [];
  const wallsLength = (mazeWidth - 1) * mazeHeight + (mazeHeight - 1) * mazeWidth;
  for (let i = 0, l = wallsLength; i < l; ++i) {
    wallsOrder.push({
      index: i + 1,
      number: Math.random(),
    });
  }

  wallsOrder.sort((wall1, wall2) => {
    if (wall1.number > wall2.number) {
      return 1;
    }

    return -1;
  });

  const resMaze = [];
  for (let j = 0; j < mazeHeight; ++j) {
    const rawMazeow = [];
    const resMazeRow = [];

    for (let i = 0; i < mazeWidth; ++i) {
      resMazeRow.push({
        wallTop: true,
        wallLeft: true,
      });
    }

    resMaze.push(resMazeRow);
  }

  for (let i = 0, l = wallsLength; i < l; ++i) {
    const {
      x,
      y,
      dx,
      dy,
    } = getCellByWallIndex(resMaze, wallsOrder[i].index);

    const solveResult = markSolve(resMaze, x, y, x + dx, y + dy);

    if (!solveResult.solved) {
      breakWall(resMaze, x, y, dx, dy);
    }
  }

  return resMaze;
}

const maze = kruskalGenerateMaze();

const cellWidth = 50;
const pathStepRadius = 20;

const twoPI = Math.PI * 2;

// Solve

function setMark(marked, x, y, value) {
  if (!marked[y]) {
    marked[y] = [];
  }

  marked[y][x] = value;
};

function getMark(marked, x, y) {
  if (!marked[y]) {
    return null;
  }

  return marked[y][x] || null;
};

function canGo(maze, x, y, dx, dy) {
  if (dx === -1) {
    return x > 0 && !maze[y][x].wallLeft;
  }

  if (dx === 1) {
    return x < mazeWidth - 1 && !maze[y][x + 1].wallLeft;
  }

  if (dy === -1) {
    return y > 0 && !maze[y][x].wallTop;
  }

  if (dy === 1) {
    return y < mazeHeight - 1 && !maze[y + 1][x].wallTop;
  }

  throw new Error(`Unknown direction (${ dx }, ${ dy })`);
};

function markSolveStep(maze, marked, x, y, depth) {
  setMark(marked, x, y, depth);

  const newMark = depth + 1;

  for (let i = 0; i < 4; ++i) {
    const newX = x + dxMethods[i];
    const newY = y + dyMethods[i];

    const oldMark = getMark(marked, newX, newY);

    if (
      (oldMark === null || newMark < oldMark) &&
      canGo(maze, x, y, dxMethods[i], dyMethods[i])
    ) {
      markSolveStep(maze, marked, newX, newY, newMark);
    }
  }
};

function getPath(maze, marked, x, y, path) {
  if (x === startX && y === startY) {
    return path;
  }

  const currentMark = getMark(marked, x, y);

  for (let i = 0; i < 4; ++i) {
    const newX = x + dxMethods[i];
    const newY = y + dyMethods[i];

    const mark = getMark(marked, newX, newY);

    if (
      mark === currentMark - 1 &&
      canGo(maze, x, y, dxMethods[i], dyMethods[i])
    ) {
      return getPath(maze, marked, newX, newY, [{
        x: newX,
        y: newY
      }].concat(path));
    }
  }
};

function markSolve(maze, startX, startY, targetX, targetY) {
  const marked = [];
  markSolveStep(maze, marked, startX, startY, 1);

  if (getMark(marked, targetX, targetY)) {
    return {
      path: getPath(maze, marked, targetX, targetY, [{
        x: targetX,
        y: targetY,
      }]),
      solved: true,
    };
  }

  return {
    solved: false,
  };
};

// Draw

function drawMaze() {
  ctx.beginPath();
  ctx.strokeStyle = 'black';

  for (let i = 0; i < mazeWidth; ++i) {
    for (let j = 0; j < mazeHeight; ++j) {
      const mazeCell = maze[j][i];

      const cornerX = i * cellWidth;
      const cornerY = j * cellWidth;

      if (mazeCell.wallLeft) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX, cornerY + cellWidth);
        ctx.stroke();
      }

      if (mazeCell.wallTop) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX + cellWidth, cornerY);
        ctx.stroke();
      }
    }
  }

  const downRightCornerX = mazeWidth * cellWidth;
  const downRightCornerY = mazeHeight * cellWidth;

  ctx.moveTo(0, downRightCornerY);
  ctx.lineTo(downRightCornerX, downRightCornerY);
  ctx.lineTo(downRightCornerX, 0);
  ctx.stroke();

  ctx.closePath();
};

function drawPath(path) {
  ctx.fillStyle = '#FF0000';
  ctx.strokeStyle = '#FF0000';
  path.forEach(({ x, y }) => {
    const cornerX = (x + 0.5) * cellWidth;
    const cornerY = (y + 0.5) * cellWidth;

    ctx.beginPath();
    ctx.arc(cornerX, cornerY, pathStepRadius, 0, twoPI);
    if (
      x === startX && y === startY ||
      x === targetX && y === targetY
    ) {
      ctx.fill();
    } else {
      ctx.stroke();
    }
    ctx.closePath();
  });
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawMaze();

  const solveResult = markSolve(maze, startX, startY, targetX, targetY);

  if (solveResult.solved) {
    drawPath(solveResult.path);
  }
}

draw();
