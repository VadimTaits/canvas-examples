// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const mazeWidth = 5;
const mazeHeight = 4;

const maze = [
  [{
    wallTop: true,
    wallLeft: true,
  }, {
    wallTop: true,
    wallLeft: true,
  }, {
    wallTop: true,
    wallLeft: false,
  }, {
    wallTop: true,
    wallLeft: true,
  }, {
    wallTop: true,
    wallLeft: false,
  }],

  [{
    wallTop: false,
    wallLeft: true,
  }, {
    wallTop: true,
    wallLeft: false,
  }, {
    wallTop: false,
    wallLeft: true,
  }, {
    wallTop: false,
    wallLeft: false,
  }, {
    wallTop: true,
    wallLeft: true,
  }],

  [{
    wallTop: false,
    wallLeft: true,
  }, {
    wallTop: true,
    wallLeft: false,
  }, {
    wallTop: true,
    wallLeft: false,
  }, {
    wallTop: false,
    wallLeft: false,
  }, {
    wallTop: false,
    wallLeft: false,
  }],

  [{
    wallTop: true,
    wallLeft: true,
  }, {
    wallTop: false,
    wallLeft: false,
  }, {
    wallTop: false,
    wallLeft: true,
  }, {
    wallTop: false,
    wallLeft: true,
  }, {
    wallTop: false,
    wallLeft: true,
  }],
];

const cellWidth = 50;
const pathStepRadius = 20;

const twoPI = Math.PI * 2;

// Solve

const visited = [];
function setVisited(x, y, value) {
  if (!visited[y]) {
    visited[y] = [];
  }

  visited[y][x] = value;
};

function checkVisited(x, y) {
  if (!visited[y]) {
    return false;
  }

  return !!visited[y][x];
};

const startX = Math.floor(Math.random() * mazeWidth);
const startY = Math.floor(Math.random() * mazeHeight);
const targetX = Math.floor(Math.random() * mazeWidth);
const targetY = Math.floor(Math.random() * mazeHeight);
const dxMethods = [1, 0, -1, 0];
const dyMethods = [0, -1, 0, 1];
const path = [];

function canGo(x, y, dx, dy) {
  if (dx === -1) {
    return x > 0 && !maze[y][x].wallLeft;
  }

  if (dx === 1) {
    return x < mazeWidth - 1 && !maze[y][x + 1].wallLeft;
  }

  if (dy === -1) {
    return y > 0 && !maze[y][x].wallTop;
  }

  if (dy === 1) {
    return y < mazeHeight - 1 && !maze[y + 1][x].wallTop;
  }

  throw new Error(`Unknown direction (${ dx }, ${ dy })`);
};

function recursiveSolveStep(x, y, oldPath) {
  setVisited(x, y, true);

  const path = oldPath.slice();

  path.push({ x, y });

  if (x === targetX && y === targetY) {
    return {
      solved: true,
      path,
    };
  }

  for (let i = 0; i < 4; ++i) {
    const newX = x + dxMethods[i];
    const newY = y + dyMethods[i];

    if (
      !checkVisited(newX, newY) &&
      canGo(x, y, dxMethods[i], dyMethods[i])
    ) {
      const result = recursiveSolveStep(newX, newY, path);

      if (result.solved) {
        return result;
      }
    }
  }

  setVisited(x, y, false);
  return {
    solved: false,
  };
};

function recursiveSolve() {
  return recursiveSolveStep(startX, startY, []);
};

// Draw

function drawMaze() {
  ctx.beginPath();
  ctx.strokeStyle = 'black';

  for (let i = 0; i < mazeWidth; ++i) {
    for (let j = 0; j < mazeHeight; ++j) {
      const mazeCell = maze[j][i];

      const cornerX = i * cellWidth;
      const cornerY = j * cellWidth;

      if (mazeCell.wallLeft) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX, cornerY + cellWidth);
        ctx.stroke();
      }

      if (mazeCell.wallTop) {
        ctx.moveTo(cornerX, cornerY);
        ctx.lineTo(cornerX + cellWidth, cornerY);
        ctx.stroke();
      }
    }
  }

  const downRightCornerX = mazeWidth * cellWidth;
  const downRightCornerY = mazeHeight * cellWidth;

  ctx.moveTo(0, downRightCornerY);
  ctx.lineTo(downRightCornerX, downRightCornerY);
  ctx.lineTo(downRightCornerX, 0);
  ctx.stroke();

  ctx.closePath();
};

function drawPath(path) {
  ctx.fillStyle = '#FF0000';
  ctx.strokeStyle = '#FF0000';
  path.forEach(({ x, y }) => {
    const cornerX = (x + 0.5) * cellWidth;
    const cornerY = (y + 0.5) * cellWidth;

    ctx.beginPath();
    ctx.arc(cornerX, cornerY, pathStepRadius, 0, twoPI);
    if (
      x === startX && y === startY ||
      x === targetX && y === targetY
    ) {
      ctx.fill();
    } else {
      ctx.stroke();
    }
    ctx.closePath();
  });
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawMaze();

  const solveResult = recursiveSolve();

  if (solveResult.solved) {
    drawPath(solveResult.path);
  }
}

draw();
