// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

let bgX = 0;
let vX = 2;

const bgImage = new Image();
bgImage.src = '/bg.jpg';

let imageWidthForDraw;

// Recount position

let oldTime = Date.now();
function recountPosition() {
  const newTime = Date.now();

  const timeDiff = newTime - oldTime;
  const dX = vX * timeDiff;

  let newX = bgX + dX;
  if (newX > bgImage.width - canvas.width) {
    newX = 2 * (bgImage.width - canvas.width) - newX;
    vX = -vX;
  } else if (newX < 0) {
    newX = -newX;
    vX = -vX;
  }

  bgX = newX;

  oldTime = newTime;
};

// Draw

function drawImage() {
  ctx.drawImage(bgImage,
    bgX, 0, imageWidthForDraw, bgImage.height,
    0, 0, canvas.width, canvas.height);
};

function draw() {
  recountPosition();

  drawImage();

  requestAnimationFrame(draw);
};

bgImage.onload = () => {
  imageWidthForDraw = canvas.width * bgImage.height / canvas.height;
  draw();
};
