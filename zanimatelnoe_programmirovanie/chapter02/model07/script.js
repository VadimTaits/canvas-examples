// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

const fakeCanvas = document.createElement('canvas');
const fakeCtx = fakeCanvas.getContext('2d');

// Variables

const bgImage = new Image();
bgImage.src = '/bg.jpg';

const lastR = 0;
const lastG = 0;
const lastB = 0;

const iterationsCount = 20;
const iterationTimeout = 25;
let currentIteration = 0;

let bgImageOffsetX;
let bgImageOffsetY;

let pixelsMap = [];

// Draw

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if (currentIteration === iterationsCount) {
    ctx.drawImage(bgImage,
      0, 0, bgImage.width, bgImage.height,
      bgImageOffsetX, bgImageOffsetY, bgImage.width, bgImage.height);

    return;
  }

  for (let j = 0, k = bgImage.height; j < k; ++j) {
    for (let i = 0, l = bgImage.width; i < l; ++i) {
      const point = pixelsMap[j][i];

      const pixel = fakeCtx.getImageData(i, j, 1, 1).data;

      const pixelImageData = ctx.createImageData(1, 1);
      pixelImageData.data[0] = pixel[0];
      pixelImageData.data[1] = pixel[1];
      pixelImageData.data[2] = pixel[2];
      pixelImageData.data[3] = pixel[3];

      ctx.putImageData(pixelImageData, point.x, point.y);

      point.x += point.dx;
      point.y += point.dy;
    }
  }

  ++currentIteration;

  setTimeout(draw, iterationTimeout);
};

bgImage.onload = () => {
  fakeCanvas.width = bgImage.width;
  fakeCanvas.height = bgImage.height;

  bgImageOffsetX = Math.round((canvas.width - bgImage.width) / 2);
  bgImageOffsetY = Math.round((canvas.height - bgImage.height) / 2);

  fakeCtx.drawImage(bgImage,
    0, 0, bgImage.width, bgImage.height,
    0, 0, bgImage.width, bgImage.height);

  for (let j = 0, k = bgImage.height; j < k; ++j) {
    const newRow = [];
    for (let i = 0, l = bgImage.width; i < l; ++i) {
      const x = Math.floor(Math.random() * canvas.width);
      const y = Math.floor(Math.random() * canvas.height);

      const dx = (bgImageOffsetX + i - x) / iterationsCount;
      const dy = (bgImageOffsetY + j - y) / iterationsCount;

      newRow.push({
        x,
        y,
        dx,
        dy,
      });
    }

    pixelsMap.push(newRow);
  }

  draw();
};
