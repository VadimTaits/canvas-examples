// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

let x = canvas.width / 2;
var y = canvas.height / 2;

let vx = (1 + Math.floor(Math.random() * 19)) / 50;
let vy = (1 + Math.floor(Math.random() * 19)) / 50;

const radius = 40;

const twoPI = Math.PI*2;

// Recount position

let oldTime = Date.now();
function recountPosition() {
  const newTime = Date.now();

  const timeDiff = newTime - oldTime;
  const dx = vx * timeDiff;
  const dy = vy * timeDiff;

  let newX = x + dx;
  if (newX > canvas.width - radius) {
    newX = 2 * (canvas.width - radius) - newX;
    vx = -vx;
  } else if (newX < radius) {
    newX = 2 * radius - newX;
    vx = -vx;
  }

  x = newX;

  let newY = y + dy;
  if (newY > canvas.height - radius) {
    newY = 2 * (canvas.height - radius) - newY;
    vy = -vy;
  } else if (newY < radius) {
    newY = 2 * radius - newY;
    vy = -vy;
  }

  y = newY;

  oldTime = newTime;
};

// Draw

function drawCircle() {
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, twoPI);
  ctx.fillStyle = "#FF0000";
  ctx.fill();
  ctx.closePath();
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawCircle();

  recountPosition();

  requestAnimationFrame(draw);
}

draw();
