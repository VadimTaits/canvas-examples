// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

let x = canvas.width / 2;
var y = canvas.height / 2;

let vx = (1 + Math.floor(Math.random() * 19)) / 50;
let vy = (1 + Math.floor(Math.random() * 19)) / 50;

const radius = 40;
const diam = 2 * radius;

const twoPI = Math.PI*2;

const bgImage = new Image();
bgImage.src = '/bg.jpg';

var spriteImage = new Image();
spriteImage.src = '/sprite.png';

// Recount position

let oldTime = Date.now();
function recountPosition() {
  const newTime = Date.now();

  const timeDiff = newTime - oldTime;
  const dx = vx * timeDiff;
  const dy = vy * timeDiff;

  let newX = x + dx;
  if (newX > canvas.width - radius) {
    newX = 2 * (canvas.width - radius) - newX;
    vx = -vx;
  } else if (newX < radius) {
    newX = 2 * radius - newX;
    vx = -vx;
  }

  x = newX;

  let newY = y + dy;
  if (newY > canvas.height - radius) {
    newY = 2 * (canvas.height - radius) - newY;
    vy = -vy;
  } else if (newY < radius) {
    newY = 2 * radius - newY;
    vy = -vy;
  }

  y = newY;

  oldTime = newTime;
};

// Draw

function clearCircle() {
  const dx = x - radius;
  const dy = y - radius;

  ctx.drawImage(bgImage,
    dx, dy, diam, diam,
    dx, dy, diam, diam);
};

function drawCircle() {
  ctx.drawImage(spriteImage,
    0, 0, spriteImage.width, spriteImage.height,
    x - radius, y - radius, diam, diam);
};

function draw() {
  clearCircle();
  recountPosition();

  drawCircle();

  requestAnimationFrame(draw);
};

bgImage.onload = () => {
  ctx.drawImage(bgImage,
    0, 0, bgImage.width, bgImage.height,
    0, 0, canvas.width, canvas.height);
};

draw();
