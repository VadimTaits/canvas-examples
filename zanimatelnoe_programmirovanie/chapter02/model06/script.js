// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const bgImage = new Image();
bgImage.src = '/bg.jpg';

const bgImage2 = new Image();
bgImage2.src = './bg2.jpg';

const imageWidth = canvas.width;
const imageHeight = canvas.height;

const changeTime = 1000;
const showTime = 1500;

const halfIterationTime = changeTime + showTime;
const iterationTime = 2 * halfIterationTime;

const startTime = Date.now();

const redrawTime = 15;

// Draw

function draw() {
  const currentTime = Date.now();

  const currentIterationOffset = (currentTime - startTime) % iterationTime;

  const isFirstImageBg = currentIterationOffset < halfIterationTime;

  let backImage;
  let topImage;
  if (isFirstImageBg) {
    backImage = bgImage2;
    topImage = bgImage;
  } else {
    backImage = bgImage;
    topImage = bgImage2;
  }

  ctx.drawImage(backImage,
    0, 0, imageWidth, imageHeight,
    0, 0, imageWidth, imageHeight);

  const currentHalfIterationOffset = currentIterationOffset % halfIterationTime;
  if (currentHalfIterationOffset < changeTime) {
    const dx = imageWidth * currentHalfIterationOffset / changeTime;
    const showWidth = imageWidth - dx;

    for (let i = 0; i < imageHeight; ++i) {
      if (i % 2 === 0) {
        ctx.drawImage(topImage,
          0, i, showWidth, 1,
          dx, i, showWidth, 1);
      } else {
        ctx.drawImage(topImage,
          dx, i, showWidth, 1,
          0, i, showWidth, 1);
      }
    }
  }

  setTimeout(draw, redrawTime);
};

const imageLoadPromise = (image) => new Promise((resolve, reject) => {
  image.onload = () => {
    resolve();
  };

  image.onerror = () => {
    reject();
  };
});

Promise.all([
  imageLoadPromise(bgImage),
  imageLoadPromise(bgImage2),
])
  .then(() => {

  });

bgImage.onload = () => {
  draw();
};
