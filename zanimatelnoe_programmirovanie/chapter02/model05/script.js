// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const bgImage = new Image();
bgImage.src = '/bg.jpg';

const lastR = 0;
const lastG = 0;
const lastB = 0;

const iterationTimeout = 25;

// Recount position

function getNewColor(target, dest) {
  if (target < dest) {
    return target + 1;
  }

  if (target > dest) {
    return target - 1;
  }

  return target;
};

function redrawPixels() {
  let hasChanges = false;

  for (let i = 270; i < 370; ++i) {
    for (let j = 190; j < 290; ++j) {
      const pixel = ctx.getImageData(i, j, 1, 1).data;

      const newR = getNewColor(pixel[0], lastR);
      const newG = getNewColor(pixel[1], lastG);
      const newB = getNewColor(pixel[2], lastB);

      const pixelCahnged = newR !== pixel[0] || newG !== pixel[1] || newB !== pixel[2];

      if (pixelCahnged) {
        const pixelImageData = ctx.createImageData(1, 1);
        pixelImageData.data[0] = newR;
        pixelImageData.data[1] = newG;
        pixelImageData.data[2] = newB;
        pixelImageData.data[3] = pixel[3];

        ctx.putImageData(pixelImageData, i ,j);
      }

      hasChanges = hasChanges || pixelCahnged;
    }
  }

  return hasChanges;
};

// Draw

function draw() {
  const hasChanges = redrawPixels();

  if (hasChanges) {
    setTimeout(draw, iterationTimeout);
  }
};

bgImage.onload = () => {
  ctx.drawImage(bgImage,
    0, 0, bgImage.width, bgImage.height,
    0, 0, canvas.width, canvas.height);

  draw();
};
