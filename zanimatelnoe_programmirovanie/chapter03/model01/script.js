// Canvas

const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

// Variables

const halfCanvasWidth = canvas.width / 2;
const halfCanvasHeight = canvas.height / 2;

const perspective = 1000;

function multMatrixes(matrix1, matrix2) {
  let res = [];

  for (let matrix1y = 0, k = matrix1.length; matrix1y < k; ++matrix1y) {
    const resRow = [];

    for (let matrix2x = 0, matrix2Width = matrix2[0].length; matrix2x < matrix2Width; ++matrix2x) {
      let cellValue = 0;
      for (let i = 0, l = matrix2.length; i < l; ++i) {
        cellValue += matrix1[matrix1y][i] * matrix2[i][matrix2x];
      }

      resRow.push(cellValue);
    }

    res.push(resRow);
  }

  return res;
};

function transposeMatrix(matrix) {
  let res = [];

  for (let i = 0, l = matrix[0].length; i < l; ++i) {
    let subRes = [];
    for (let j = 0, k = matrix.length; j < k; ++j) {
      subRes.push(matrix[j][i]);
    }

    res.push(subRes);
  }

  return res;
};

function generateMoveMatrix(dx, dy, dz) {
  return [
    [1, 0, 0, dx],
    [0, 1, 0, dy],
    [0, 0, 1, dz],
    [0, 0, 0, 1],
  ];
};

function generateRotateXMatrix(alpha) {
  const sin = Math.sin(alpha);
  const cos = Math.cos(alpha);

  return [
    [1, 0, 0, 0],
    [0, cos, -sin, 0],
    [0, sin, cos, 0],
    [0, 0, 0, 1],
  ];
};

function generateRotateYMatrix(alpha) {
  const sin = Math.sin(alpha);
  const cos = Math.cos(alpha);

  return [
    [cos, 0, sin, 0],
    [0, 1, 0, 0],
    [-sin, 0, cos, 0],
    [0, 0, 0, 1],
  ];
};

function generateRotateZMatrix(alpha) {
  const sin = Math.sin(alpha);
  const cos = Math.cos(alpha);

  return [
    [cos, -sin, 0, 0],
    [sin, cos, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1],
  ];
};

function generateResizeMatrix(kx, ky, kz) {
  return [
    [kx, 0, 0, 0],
    [0, ky, 0, 0],
    [0, 0, kz, 0],
    [0, 0, 0, 1],
  ];
};

const obj = {
  vertices: [
    [-100, -100, 0],
    [-100, 100, 0],
    [100, 100, 0],
    [100, -100, 0],
    [0, 0, -141.42],
    [0, 0, 141.42],
  ],
  edges: [
    [0, 1],
    [1, 2],
    [2, 3],
    [3, 0],
    [4, 0],
    [4, 1],
    [4, 2],
    [4, 3],
    [5, 0],
    [5, 1],
    [5, 2],
    [5, 3],
  ],
  center: [
    0,
    0,
    50,
  ],
};

const radius = 10;

const twoPI = Math.PI*2;

// Recount position

const changeMatrix = multMatrixes(
  multMatrixes(
    generateRotateXMatrix(Math.PI / 16),
    generateRotateYMatrix(Math.PI / 32)
  ),
  generateRotateZMatrix(Math.PI / 64)
);

let vx = 5;
let vy = 7;
let vz = 10;
function recountPosition(obj) {
  obj.center = [
    obj.center[0] + vx,
    obj.center[1] + vy,
    obj.center[2] + vz,
  ];

  if (obj.center[0] > halfCanvasWidth) {
    obj.center[0] = halfCanvasWidth;
    vx = -Math.abs(vx);
  } else if (obj.center[0] < -halfCanvasWidth) {
    obj.center[0] = -halfCanvasWidth;
    vx = Math.abs(vx);
  }

  if (obj.center[1] > halfCanvasHeight) {
    obj.center[1] = halfCanvasHeight;
    vy = -Math.abs(vy);
  } else if (obj.center[1] < -halfCanvasHeight) {
    obj.center[1] = -halfCanvasHeight;
    vy = Math.abs(vy);
  }

  if (obj.center[2] > 900) {
    obj.center[2] = 900;
    vz = -Math.abs(vz);
  } else if (obj.center[2] < 200) {
    obj.center[2] = 200;
    vz = Math.abs(vz);
  }

  obj.vertices = obj.vertices.map((vertex) =>
    multMatrixes([[vertex[0], vertex[1], vertex[2], 1]], changeMatrix)[0]);
};

// Draw

function toCanvasCoordinates(relativePoint, center) {
  const point = [
    relativePoint[0] + center[0],
    relativePoint[1] + center[1],
    relativePoint[2] + center[2],
  ];

  if (point[2] <= 0 || point[2] > perspective) {
    return [null, null, false];
  }

  return [
    halfCanvasWidth + perspective * point[0] / point[2],
    halfCanvasHeight + perspective * point[1] / point[2],
    true,
  ];
};

function drawShape(obj) {
  for (let i = 0, l = obj.edges.length; i < l; ++i) {
    const edge = obj.edges[i];

    const pointFrom = toCanvasCoordinates(
      obj.vertices[edge[0]],
      obj.center);
    const pointTo = toCanvasCoordinates(
      obj.vertices[edge[1]],
      obj.center);

    if (pointFrom[2] && pointTo[2]) {
        ctx.beginPath();
        ctx.strokeStyle = '#0095DD';
        ctx.moveTo(pointFrom[0], pointFrom[1]);
        ctx.lineTo(pointTo[0], pointTo[1]);
        ctx.stroke();
    }
  }
};

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawShape(obj);
  recountPosition(obj);

  setTimeout(draw, 30);
}

draw();
